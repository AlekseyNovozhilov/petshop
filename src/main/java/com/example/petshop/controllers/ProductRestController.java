package com.example.petshop.controllers;

import com.example.petshop.controllers.api.rest.ProductRestApi;
import com.example.petshop.dto.ProductDTO;
import com.example.petshop.entities.Product;
import com.example.petshop.services.Interface.ProductService;
import com.example.petshop.util.mappers.ProductMappers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductRestController implements ProductRestApi {

    private final ProductService productService;
    private final ProductMappers productMapper;

    @Override
    public ResponseEntity<Page<Product>> getAll(Integer page, Integer size) {
        Page<Product> product = productService.getAll(page, size);
        if (product.isEmpty()) {
            log.error("pageable is empty");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Content pageable: " + product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProductDTO> create(ProductDTO productDTO) {
        if (productDTO == null) {
            log.error("create: product already exist in database");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("create: new product added");
        return new ResponseEntity<>(new ProductDTO(productService.save(productMapper.convertToProductEntity(productDTO))), HttpStatus.OK);
    }
}

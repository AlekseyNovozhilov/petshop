"use client";
import ProductsCategory from "./ProductsCategory";
import styled from "styled-components";

const TitleList = styled.h3({
  fontSize: "28px",
  paddingInline: "4rem",
});

const PopularCategory = () => {
  return (
    <>
      <TitleList>Популярные категории</TitleList>
      <ProductsCategory />
    </>
  );
};

export default PopularCategory;

import React, { useState } from "react";
import styled from "styled-components";

const CustomInput = styled.input({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  padding: "0.75rem",
  // gap: 191px;

  width: "30.125rem",
  height: "2.75rem",

  border: "1px solid #A0A3BC",
  borderRadius: "0.75rem",

  // /* Inside auto layout */

  // flex: none;
  // order: 1;
  // flex-grow: 0;
});

const TextInput = () => {
  const [text, setText] = useState<string>("");
  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };
  return <CustomInput placeholder="Я ищу" value={text} onChange={changeHandler} />;
};

export default TextInput;

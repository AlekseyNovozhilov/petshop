import Image from "next/image";
import React from "react";

const Slider = () => {
  return (
    <div className="flex w-full justify-center rounded-xl">
      <Image
        width={344}
        height={189}
        src="/src/assets/Rectangle 303.png"
        alt="Working progres"
      />
    </div>
  );
};

export default Slider;

"use client";
import Logo from "@/assets/Logo";
import TextInput from "@/commons/TextInput/TextInput";
import NavMenu from "@/commons/icons/NavMenu";
import SearchIcon from "@/commons/icons/SearchIcon";
import Link from "next/link";
import React from "react";
import styled from "styled-components";

const Button = styled.button({
  borderRadius: "50%",
  width: "4rem",
  height: "4rem",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  ":hover": { backgroundColor: "rgb(241 245 249)" },
});
const CustomNav = styled.nav({
  display: "flex",
  justifyContent: "space-around",
  alignItems: "center",
  backgroundColor: "rgb(248 250 252)",
});

const NavBar = () => {
  return (
    <CustomNav>
      <Button>
        <NavMenu />
      </Button>
      <Link className="hover:bg-slate-100 px-2 py-2 rounded-lg" href="/my">
        <Logo />
      </Link>
      <TextInput />
      <Button>
        <SearchIcon />
      </Button>
    </CustomNav>
  );
};

export default NavBar;

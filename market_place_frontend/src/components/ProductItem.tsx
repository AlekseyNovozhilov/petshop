"use client";
import ProductIcon from "@/commons/icons/ProductIcon";
import React, { FC } from "react";
import styled from "styled-components";

const Button = styled.button({
  display: "flex",
  flexDirection: "column",
  backgroundColor: "#F1F1F1",
  justifyContent: "flex-start",
  alignItems: "center",
  width: "13.0625rem",
  height: "12.75rem",
  paddingBottom: "1.5rem",
  paddingTop: "1.5rem",
  gap: "12px",
  border: "1px solid #DE5703",
  borderRadius: "20px",
  ":hover": { backgroundColor: "rgb(241 245 249)" },
});

interface IButtonProductProps {
  id: number;
  name: string;
}

const ProductItem: FC<IButtonProductProps> = (props) => {
  const { name } = props;
  return (
    <Button>
      <ProductIcon />
      <p>{name}</p>
    </Button>
  );
};

export default ProductItem;

package com.example.petshop.dto;

import com.example.petshop.entities.Category;
import com.example.petshop.entities.Product;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@JsonTypeName(value = "product")
public class ProductDTO {
    private String name;
    private String brand;
    Set<Category> categories;
    private String description;
    private Double price;
    private Integer quantity;
    private Double rating;


    public ProductDTO(Product product) {
        this.name = product.getName();
        this.brand = product.getBrand();
        this.categories = product.getCategories();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.quantity = product.getQuantity();
        this.rating = product.getRating();
    }
}

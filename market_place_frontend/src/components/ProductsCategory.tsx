"use client";
import React from "react";
import ProductItem from "./ProductItem";
import styled from "styled-components";

const products = [
  {
    id: 1,
    name: "Шаблон 1",
  },
  {
    id: 2,
    name: "Шаблон 2",
  },
  {
    id: 3,
    name: "Шаблон 3",
  },
  {
    id: 4,
    name: "Шаблон 4",
  },
  {
    id: 5,
    name: "Шаблон 5",
  },
];

const ProductList = styled.ul({
  display: "flex",
  width: "80vw",
  flexWrap: "wrap",
  justifyContent: "center",
});

const ProductsCategory = () => {
  return (
    <>
      <ProductList>
        {products.map((product) => (
          <li key={product.id}>
            <ProductItem {...product} />
          </li>
        ))}
      </ProductList>
    </>
  );
};

export default ProductsCategory;

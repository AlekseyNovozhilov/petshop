INSERT INTO public.products (id,brand,description,name,price,quantity,rating) VALUES
	 (2,'NewBrand','Description','Cat food',9.99,5,5.0),
	 (1,'Brand','Description','Dog food',19.99,10,9.0),
	 (3,'BestBrand','Description','Bird food',2.99,15,7.0),
	 (4,'SuperBestBrend','Description','Food for all animals',999.99,100,10.0);

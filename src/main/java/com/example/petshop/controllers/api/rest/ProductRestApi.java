package com.example.petshop.controllers.api.rest;

import com.example.petshop.dto.ProductDTO;
import com.example.petshop.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/product")
public interface ProductRestApi {

    @GetMapping
    ResponseEntity<Page<Product>> getAll(
            @PageableDefault(sort = "name")
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size

    );

    @PostMapping
    ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO);




}

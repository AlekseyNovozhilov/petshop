package com.example.petshop.services.Interface;

import com.example.petshop.entities.Product;
import org.springframework.data.domain.Page;

public interface ProductService {

    Page<Product> getAll(int page, int size);

    Product save(Product product);
}

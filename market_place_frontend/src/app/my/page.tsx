import NavBar from "@/components/NavBar";
import PopularCategory from "@/components/PopularCategory";
import Slider from "@/components/Slider";

const MainPage = () => {
  return (
    <div>
      <NavBar />
      <Slider />
      <PopularCategory />
    </div>
  );
};

export default MainPage;

package com.example.petshop.util.mappers;

import com.example.petshop.dto.ProductDTO;
import com.example.petshop.entities.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMappers {

    public Product convertToProductEntity(ProductDTO productDTO) {
        Product product = new Product();
        product.setBrand(productDTO.getBrand());
        product.setDescription(productDTO.getDescription());
        product.setName(productDTO.getName());
        product.setPrice(productDTO.getPrice());
        product.setQuantity(productDTO.getQuantity());
        product.setRating(productDTO.getRating());
        product.setCategories(productDTO.getCategories());
        return product;
    }
}
